# Gritweb
Gritweb is an alternative web interface for git repositories to gitweb, cgit, etc. It is written in Ruby, using the Sinatra web framework and the Grit gem to access git repositories.

## Requirements
```
- python2 + pygments
- ruby
- see Install
```

## Install
Run `bundle install` in the app folder, then modify the `$repos_dir` variable in app.rb to match your repositories folder.  
Then simply run `ruby app.rb`.

## To do
Gritweb lacks refs handling. So you will only be able to use the 'master' branch, and no tags.
 
## License
See `LICENSE`