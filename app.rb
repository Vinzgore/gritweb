#! /usr/bin/ruby

require 'sinatra'
require 'sinatra/reloader'
require 'sinatra/content_for'
require 'pygments'
require 'slim'
require 'grit'

def to_pretty(d)
	a = (Time.now-d).to_i

	case a
	  when 0 then 'just now'
	  when 1 then 'a second'
	  when 2..59 then a.to_s+' seconds' 
	  when 60..119 then 'a minute' 
	  when 120..3540 then (a/60).to_i.to_s+' minutes'
	  when 3541..7100 then 'an hour' 
	  when 7101..82800 then ((a+99)/3600).to_i.to_s+' hours' 
	  when 82801..172000 then 'a day' 
	  when 172001..518400 then ((a+800)/(60*60*24)).to_i.to_s+' days'
	  when 518400..1036800 then 'a week'
	  else ((a+180000)/(60*60*24*7)).to_i.to_s+' weeks'
	end
end

configure do
	set :public_folder, Dir.pwd + '/static'
	set :port, 8080
	$repos_dir = "./repos"
end

get '/' do
	@title = "Home"
	@repos = Dir.entries($repos_dir).select { |entry|
		if File.directory? File.join($repos_dir,entry) and !(entry =='.' || entry == '..') 
			File.exist?("#{$repos_dir}/#{entry}/.git")
		end
	}

	slim :index
end

get '/about' do
	@title = "About"

	slim :about
end

get '/code' do
	@title = "Code"

	slim :code
end

get '/:repo/' do
	@title = params[:repo]

	begin
		@repo = Grit::Repo.new("#{$repos_dir}/#{params[:repo]}")
		@commits = @repo.commits

		if File.file?("#{$repos_dir}/#{params[:repo]}/README")
			@readme = File.open("#{$repos_dir}/#{params[:repo]}/README")
		elsif File.file?("#{$repos_dir}/#{params[:repo]}/README.md")
			@readme = File.open("#{$repos_dir}/#{params[:repo]}/README.md")
		elsif File.file?("#{$repos_dir}/#{params[:repo]}/README.mkdn")
			@readme = File.open("#{$repos_dir}/#{params[:repo]}/README.mkdn")
		elsif File.file?("#{$repos_dir}/#{params[:repo]}/README.markdown")
			@readme = File.open("#{$repos_dir}/#{params[:repo]}/README.markdown")
		end	
	rescue Grit::NoSuchPathError => e		
		halt 404, slim(:e404).to_s
	rescue Grit::InvalidGitRepositoryError => e
		halt 404, slim(:e404).to_s
	end

	slim :repo_summary
end

get '/:repo/log/' do
	@title = params[:repo] + " / log"
	@page = params[:page] || 1
	begin
		@repo = Grit::Repo.new("#{$repos_dir}/#{params[:repo]}")
		@commits = @repo.commits('master', 21, (@page.to_i - 1) * 21)

		if @commits == []
			redirect "/#{params[:repo]}/log/?page=#{@page.to_i - 1}"
		elsif @page.to_i < 1
			redirect "/#{params[:repo]}/log/"			
		end
	rescue Grit::NoSuchPathError => e		
		halt 404, slim(:e404).to_s
	rescue Grit::InvalidGitRepositoryError => e
		halt 404, slim(:e404).to_s
	end

	slim :repo_log
end

get '/:repo/tree/*' do
	@title = params[:repo] + " / tree"
	begin
		@repo = Grit::Repo.new("#{$repos_dir}/#{params[:repo]}")
		if params[:splat] == [""]
			@tree = @repo.tree()
		else
			@breadcrumb = params[:splat][0].split('/')
			@tree = @repo.tree()/params[:splat][0]
		end
	rescue Grit::NoSuchPathError => e		
		halt 404, slim(:e404).to_s
	rescue Grit::InvalidGitRepositoryError => e
		halt 404, slim(:e404).to_s
	end

	if @tree.inspect =~ /Grit::Blob/
		slim :repo_file
	else
		slim :repo_tree
	end
end

get '/:repo/commit/:hash?' do
	@title = params[:repo] + " / commit"
	begin
		@repo = Grit::Repo.new("#{$repos_dir}/#{params[:repo]}")
		if params[:hash]
			if @commit = @repo.commit(params[:hash])
			else
				@commit = @repo.commits.first
			end
		else
			@commit = @repo.commits.first
		end
	rescue Grit::NoSuchPathError => e		
		halt 404, slim(:e404).to_s
	rescue Grit::InvalidGitRepositoryError => e
		halt 404, slim(:e404).to_s
	end

	slim :repo_commit
end

get '/:repo/plain/*' do
	begin
		@repo = Grit::Repo.new("#{$repos_dir}/#{params[:repo]}")
		if params[:splat] == [""]
			redirect "/#{@repo}/tree/"
		else
			@tree = @repo.tree()/params[:splat][0]
		end

		if @tree.inspect =~ /Grit::Blob/
			send_file("#{$repos_dir}/#{params[:repo]}/#{params[:splat][0]}", {type: 'text/plain'})
		else
			redirect "/#{@repo}/tree/"
		end
	rescue Grit::NoSuchPathError => e		
		halt 404, slim(:e404).to_s
	rescue Grit::InvalidGitRepositoryError => e
		halt 404, slim(:e404).to_s
	end
end

get '/:repo/tarball' do
	begin
		@repo = Grit::Repo.new("#{$repos_dir}/#{params[:repo]}")

		response.headers['content_type'] = "application/gzip"
		attachment("#{params[:repo]}-master.tar.gz")
		response.write(@repo.archive_tar_gz)
	rescue Grit::NoSuchPathError => e		
		halt 404, slim(:e404).to_s
	rescue Grit::InvalidGitRepositoryError => e
		halt 404, slim(:e404).to_s
	end
end
